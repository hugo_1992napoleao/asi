import re
person = "Jose Maria Amalia, 00351 962341234, 1997-12-19"

pattern = re.compile(r'^([A-Za-z]+\s[A-Za-z]+\s[A-Za-z]*),\s00351\s(9\d{8}),\s(\d\d\d\d)-(\d\d)-(\d\d)$')

g = pattern.match(person)

if int(g.group(3)) < 2018 and int(g.group(4)) < 13 and int(g.group(5)) < 32:
    print("pessoa valida")
else:
    print("pessoa invalida")