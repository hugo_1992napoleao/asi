
def ler(nome_ficheiro):
    condutores = {}
    veiculos = {}

    with open(nome_ficheiro) as fp:
        for linha in fp:
            linha = linha.strip().split(";")
            if linha[0] in condutores.keys():
                condutores[linha[0]].append(linha[1])
            else:
                matriculas = [linha[1]]
                condutores[linha[0]] = matriculas
            if linha[1] in veiculos.keys():
                veiculos[linha[1]] [linha[2]] = [linha[3], linha[4], linha[5]]
            else:
                contra_ordenacoes = {}
                contra_ordenacoes[linha[2]] = [linha[3], linha[4], linha[5]]
                veiculos[linha[1]] = contra_ordenacoes
    return condutores, veiculos