
def acumulado(condutores, veiculos):
    acumulado = {}
    for condutor in condutores.keys():
        valor = 0
        for matricula in set(condutores[condutor]):
            for data in veiculos[matricula].keys():
                valor = valor + float(veiculos[matricula][data][1])
        acumulado[condutor] = valor
    return acumulado
