import re
ip = "192.168.1.0/24"
ip2 = "10.10.10.0/16"

pattern = re.compile(r'^(\d\d\d\d)\/(\d\d)\/(\d\d)\s(\d\d):(\d\d):(\d\d)$')

g = pattern.match(ip)

if int(g.group(1)) < 2018 and int(g.group(2)) < 12 and int(g.group(3)) < 31 and int(g.group(4)) < 24 and \
        int(g.group(5)) < 60 and int(g.group(6)) < 60:
    print("IP valido")
else:
    print("IP invalido")

