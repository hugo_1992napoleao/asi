ages = [16, 18, 10, 28, 24, 26, 30, 46, 72, 65, 91]
# A idade mais nova do array
minAge = 999
for age in ages:
    if minAge > age:
        minAge = age
print(minAge)

# A idade mais velha do vetor
maxAge = 0
for age in ages:
    if maxAge < age:
        maxAge = age
print(maxAge)

# A media das idades
media = 0
soma = 0
size = len(ages)
for age in ages:
    soma = soma + age
media = soma / size
print(media)

# A media das idades entre 18 e 65

media = 0
soma = 0
count = 0
for age in ages:
    if (age > 18) and (age < 65):
        soma = soma + age
        count += 1
media = soma / count
print(media)
