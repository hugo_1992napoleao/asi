import datetime

Ages = {'criança': [0,12], 'juvenil': [13,17], 'adulto': [18,64], 'sénior': [65]}
listAges = ['12/08/2008', '21/06/1948', '05/06/2003', '03/01/1979', '21/05/2012']

for listAge in listAges:
    date_time_obj = datetime.datetime.strptime(listAge, '%d/%m/%Y')
    if 2018 - date_time_obj.year <= 12:
        print('criança ', date_time_obj.date())
    elif 2018 - date_time_obj.year >= 13 and 2018 - date_time_obj.year <= 17:
        print('juvenil ', date_time_obj.date())
    elif 2018 - date_time_obj.year >= 18 and 2018 - date_time_obj.year <= 64:
        print('adulto ', date_time_obj.date())
    elif 2018 - date_time_obj.year >= 65:
        print('sénior ', date_time_obj.date())