veiculos = {}
imoveis = {}
maxTaxIMI = 0.0


with open("Veiculos.txt") as fp:
    for row in fp:
        row = row.strip()
        row = row.split(";")
        imoveis[row[0]] = {row[4]: row[6]}
        veiculos[row[0]] = {row[2]: row[3]}

for p_id, p_info in imoveis.items():
    for key in p_info:
        if maxTaxIMI < float(p_info[key]):
            maxTaxIMI = float(p_info[key])

print("O valor de imposto mais alto é de", + maxTaxIMI)
