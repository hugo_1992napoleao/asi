students = {}

with open("Input.txt") as fp:
    for linha in fp:
        linha = linha.split(';')
        students[linha[0]] = linha[1]

number = input('Numero do estudante: ')
print('O aluno com o numero  %s chama-se %s' % (number, students[number]))
